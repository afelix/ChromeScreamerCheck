/**
 * Created by afelix on 24/08/15.
 */

var shorteners = [
    'http://bit.ly',
    'http://tinyurl.com',
    'http://goo.gl',
    'http://t.co',
    'https://t.co',
    'http://adf.ly',
    'http://ow.ly',
    'http://tr.im'
];


document.addEventListener('mousemove', function(e) {
    var target_url = '';
    var hover_element = e.srcElement;

    if (hover_element.nodeName == 'A') {
        target_url = hover_element.href;
    }
    else {
        if (hover_element.parentNode.nodeName == 'A') {
            target_url = hover_element.parentNode.href;
        }
    }

    if (target_url != '') {
        for (var i = 0; i < shorteners.length; i++) {
            if (target_url.startsWidth(shorteners[i])) { //Protip: do not test with ancient browser... :+
                console.log('Shortener found: ' + target_url);
            }
        }
    }
}, false);